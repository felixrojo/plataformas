import pygame as pg
import sys
import variables_globales as gb

class Game:
    def __init__(self):
        pg.init()
        self.pantalla = pg.display.set_mode(gb.RESOLUCION)
        self.clock = pg.time.Clock()
        self.programaEjecutandose = True
    
    def check_event(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
    
    def update(self):
        pg.display.set_caption(str(int(self.clock.get_fps())))
        pg.display.flip()
        self.clock.tick(gb.FPS)
        
    def buclePrincipal(self):
        while self.programaEjecutandose:
            self.check_event()
            self.update()
            



if __name__ == '__main__':
    game = Game()
    game.buclePrincipal()